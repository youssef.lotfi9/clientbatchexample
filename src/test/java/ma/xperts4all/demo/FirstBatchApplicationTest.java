package ma.xperts4all.demo;

import ma.xperts4all.demo.config.SpringBatchConfig;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;

@RunWith(SpringRunner.class)
@SpringBatchTest
@EnableAutoConfiguration
@ContextConfiguration(classes = {SpringBatchConfig.class})
@SpringBootTest
public class FirstBatchApplicationTest {

    private static final String TEST_OUTPUT = "src/test/resources/output.test/actual-output.test.csv";

//    private static final String EXPECTED_OUTPUT = "src/test/resources/output.test/expected-output.test.json";

    private static final String TEST_INPUT = "C:\\Users\\PC-LTF\\Desktop\\Xperts4All\\batch\\my demo batch\\src\\test\\java\\resources\\input\\test-input.csv";

    @Test
    public void test1() {
        try {
            File file = new File(TEST_INPUT);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            String[] tempArr;
            while ((line = br.readLine()) != null) {
                tempArr = line.split("\n");
                for (String tempStr : tempArr) {
                    System.out.print(tempStr + " ");
                }
                System.out.println();
            }
            br.close();
            System.out.println("done!!");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        Assert.assertEquals("hi", "hi");
    }


}
