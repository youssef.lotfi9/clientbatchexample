package ma.xperts4all.demo.entity;

public class Client {
    private String firstName;

    private String lastName;

    private STATUS status;

    private Long balance;

    private Long amount;

    private boolean done;

    public Client() {
    }

    public Client(String firstName, String lastName, STATUS status, Long balance, Long amount, boolean done) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.balance = balance;
        this.amount = amount;
        this.done = done;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return "Client{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", status=" + status +
                ", balance=" + balance +
                ", amount=" + amount +
                ", done=" + done +
                '}';
    }
}
