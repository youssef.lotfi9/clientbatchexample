package ma.xperts4all.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstBatchApplication.class, args);
	}

	/* TODO
	* student: id , name, date of birth ,
	* task 1: read data from csv and save it to db , with
	* - age is being calculated
	* - chars to maj */

}
