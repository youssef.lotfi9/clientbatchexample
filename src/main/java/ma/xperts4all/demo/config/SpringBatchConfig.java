package ma.xperts4all.demo.config;

import ma.xperts4all.demo.entity.Client;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;

import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.PathResource;


@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    /* todo - file to db
            - item reader ✔
            - item writer ✔
            - process ✔
            - step ✔
            - job ✔
      */

    // 1- item reader
    @Bean
    public FlatFileItemReader<Client> readAllClients() {
        return new FlatFileItemReaderBuilder<Client>()
                .name("readAllClients")
                .resource(new ClassPathResource("Transfers.csv"))
                .linesToSkip(1)
                .delimited()
                .names(new String[]{"first name", "last name", "status", "balance", "amount", "done"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                    setTargetType(Client.class);
                }})
                .build();
    }


    @Bean
    public ClientProcess clientProcess() {
        return new ClientProcess();
    }

    // writer
    @Bean
    public FlatFileItemWriter<Client> writReceiversClients(@Value("${source-output}") String output) {
        return new FlatFileItemWriterBuilder<Client>()
                .name("writeClients")
                .resource(new FileSystemResource(output))
                .append(false)
                .delimited()
                .names(new String[]{"firstName", "lastName", "status", "balance", "amount", "done"})
                .build();
    }

    //  step
    @Bean
    public Step step(@Value("${source-output}") String output) {
        return stepBuilderFactory.get("step")
                .<Client, Client>chunk(5)
                .reader(readAllClients())
                .processor(clientProcess())
                .writer(writReceiversClients(output))
                .build();
    }

    @Bean
    public Job job1(Step step) {

        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step)
                .end()
                .build();
    }

}
