package ma.xperts4all.demo.config;

import ma.xperts4all.demo.entity.Client;
import ma.xperts4all.demo.entity.STATUS;
import org.springframework.batch.item.ItemProcessor;

public class ClientProcess implements ItemProcessor<Client, Client> {
    @Override
    public Client process(Client item) throws Exception {
        final String firstName = item.getFirstName();
        final String lastName = item.getLastName();
        final STATUS status = item.getStatus();
        final Long balance = item.getBalance();
        final Long amount = item.getAmount();
        final boolean isDone = true;

        return new Client(firstName, lastName, status, balance, amount, isDone);
    }
}
